# Training Selenium 201903

URL Aplikasi Under Test :

* [Form Input](https://training-selenium-201903.herokuapp.com/tamu/form)
* [List Data](https://training-selenium-201903.herokuapp.com/tamu/list)

Info Koneksi Database MySQL

* Username : `b5428b090521cd`
* Password : `a3c84f29`
* Host Server : `us-cdbr-iron-east-02.cleardb.net`
* Nama Database : `heroku_d038fb2ff522d9d`

Setting Koneksi Database Local

* Username : `bukutamu`
* Password : `bukutamu123`
* Host Server : `localhost`
* Nama Database : `bukutamudb`

Cara membuat username database di localhost

```
grant all on bukutamudb.* to bukutamu@localhost identified by 'bukutamu123';
```

Cara membuat database di localhost

```
create database bukutamudb;
```

Cara menjalankan aplikasi

```
mvn spring-boot:run
```



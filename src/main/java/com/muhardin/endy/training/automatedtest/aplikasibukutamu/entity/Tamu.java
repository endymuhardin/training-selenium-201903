package com.muhardin.endy.training.automatedtest.aplikasibukutamu.entity;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

@Entity @Data
public class Tamu {
    
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @NotEmpty @Size(min = 3, max = 255)
    private String nama;
    
    @NotEmpty @Email
    private String email;
    
    @NotEmpty @Size(min = 5)
    private String noHp;
    private String alamat;
    
    @NotNull
    @Enumerated(EnumType.STRING)
    private JenisKelamin jenisKelamin;
    
    @NotNull
    @Enumerated(EnumType.STRING)
    private Pendidikan pendidikan;
    
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalLahir;
}

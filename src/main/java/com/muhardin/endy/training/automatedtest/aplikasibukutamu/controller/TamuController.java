package com.muhardin.endy.training.automatedtest.aplikasibukutamu.controller;

import com.muhardin.endy.training.automatedtest.aplikasibukutamu.dao.TamuDao;
import com.muhardin.endy.training.automatedtest.aplikasibukutamu.entity.Tamu;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

@Controller
public class TamuController {
    
    @Autowired private TamuDao tamuDao;

    @DeleteMapping("/tamu/reset")
    @ResponseStatus(HttpStatus.OK)
    public void resetDatabase() {
        tamuDao.deleteAll();
    }

    @GetMapping("/tamu/list")
    public ModelMap dataTamu(Pageable pgbl){
        ModelMap mm = new ModelMap();
        
        Page<Tamu> hasilQueryDb = tamuDao.findAll(pgbl);
        mm.addAttribute("dataTamu", hasilQueryDb);
        
        return mm;
    }
    
    @GetMapping("/tamu/form")
    public ModelMap tampilkanForm(){
        return new ModelMap().addAttribute(new Tamu());
    }
    
    @PostMapping("/tamu/form")
    public String prosesForm(@ModelAttribute @Valid Tamu t, BindingResult errors, SessionStatus status){
        if(errors.hasErrors()){
            return "tamu/form";
        }
        tamuDao.save(t);
        status.setComplete();
        return "redirect:list";
    }
}
